package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> FridgeList = new ArrayList<>();
    
    
    int max_size = 20;
    @Override
    public int totalSize(){
        return  max_size;
    }
    @Override
    public int nItemsInFridge() {
        return FridgeList.size();
    }
    @Override
    public boolean placeIn(FridgeItem item) {
        System.out.println(FridgeList.isEmpty());
        if(FridgeList.size()<20 || FridgeList.isEmpty()){
            FridgeList.add(item);
            return true;
        }
        return false;
    }
    @Override
    public void takeOut(FridgeItem item) {
        if(!FridgeList.contains(item)){
            throw new NoSuchElementException("The fridge does not contain: " + item.getName());
        }
        else{
            FridgeList.remove(item);
        }
    }
    @Override
    public void emptyFridge() {
        //Bruk heller .clear? remove all bytter kun ut med null verdier//gløm den?
        FridgeList.removeAll(FridgeList);
    }
    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>(); 
        //expiredFood.removeAll(expiredFood);

        for(FridgeItem item : FridgeList){
            if(item.hasExpired()){
                expiredFood.add(item);
            }
        }
        FridgeList.removeAll(expiredFood);
        return expiredFood;
    }

}


